// Youtube crawler for videos
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var https = require('https');
var querystring = require('querystring');
var _ = require('lodash');
var objectAssign = require('object-assign');

mongoose.Promise = global.Promise;

var help = process.argv[1] +  "--apiKey [Youtube Api Key]";

var myargs = process.argv.slice(2);

var namedArgs = _.chunk(myargs,2);

var objArgs = {};

_.each(namedArgs,function(val){
	// strip the -- from arg name
	var name = val[0].replace(/-/g,'').toLowerCase();
	objArgs[name] = val[1];
});

if(!('apikey' in objArgs)){
	console.log('Missing API key');
	console.log(help);
	process.exit(1);
}

// Model dec's 
var Category, Video;

var db = mongoose.connection;

db.on('error', console.error);


var connStr = 'mongodb://localhost:27017/indietv';

mongoose.connect(connStr, function(err) {
    if (err) throw err;
    console.log('Successfully connected to MongoDB');
});

var categorySchema = new Schema({
	categoryId: {type: String, required:true, unique: true},
	categoryName : {type: String},
	syncDate : {type:Date}
});

var videoSchema = new Schema({
	videoId: {type: String, required: true, unique: true},
	url: {type:String},
	title:{type:String},
	synopsis: {type:String},
	publishDate: {type:Date},
	length: {type:Number},
	channelId: {type:String},
	categoryId:{type:String},
	syncDate: {type:Date}
});  

Category = db.model('VideoCategory',categorySchema,'categories');
Video = db.model('Video', videoSchema,'videos');	


// have we synched categories in the last 30 days?
var today = new Date();
var categoryProcessing = Category.count({syncDate:{'$gte':today.setDate(today.getDate() + -30)} }).exec();

categoryProcessing.then(
	function(result){
		console.log('inside exec');
		if(!result){
			var data = {part:'snippet', regionCode:'US',key: objArgs.apikey};
			var endPoint = '/youtube/v3/videoCategories';
			performRequest('www.googleapis.com',endPoint,'GET',data,function(jsonObj){
				console.log('received category response');
				// Create the category objects
				var cats = [];
				_.each(jsonObj.items,function(el){
					cats.push(new Category({categoryId:el.id, categoryName:el.snippet.title, syncDate: new Date()}));
				});
				// Remove old categories
				Category.remove({});
				// Insert new items
				Category.create(cats);
			});
		}
	
	// Either way return a promise so that we can chain it.
	return Category.find({categoryId:{'$in':['1','18','19','24','26']}}).select({'categoryId':1,'_id':0});
})
.then(function(rs2){
	var promise = null;
	// process each category
	_.each(rs2,function(cat){

		var data = {
			part:'snippet', 
			regionCode:'US',
			key: objArgs.apikey,
			maxResults:20,
			order:'rating',
			regionCode:'US',
			relevanceLanguage:'EN',
			type:'video',
			videoCaption:'none',
			videoCategoryId:cat.categoryId,
			videoDuration:'long',
			videoEmbeddable:true
			};		
			var endPoint = '/youtube/v3/search';
			console.log('Calling video search with category %d',cat.categoryId);
			performRequest('www.googleapis.com',endPoint,'GET',data,function(jsonObj){
				var actions = [];
				// Create the video objects
				_.each(jsonObj.items,function(el){
					var video = new Video(
					{
						videoId:el.id.videoId, 
						url:'https://www.youtube.com/watch?v=' + el.id.videoId, 
						title: el.snippet.title,
						channelId:el.snippet.channelId,
						synopsis:el.snippet.description,
						publishDate:el.snippet.publishedAt,
						categoryId: cat.categoryId,
						syncDate: new Date()
					});
					var videoToUpdate = {};
					videoToUpdate = objectAssign(videoToUpdate,video._doc);
					delete videoToUpdate._id;
					// push the promise into array for a promises all.
					actions.push({query: {videoId: video.videoId}, data: videoToUpdate});					
				});
				// build an array of promises.
				//promises = actions.map(function(arr){
				//	return Video.findOneAndUpdate(arr.query, arr.data, {upsert:true}).exec();			
				//});
				promise = new Promise(function(resolve, reject){
					var bulk = Video.collection.initializeUnorderedBulkOp();
					actions.forEach(function(record){
					  bulk.find(record.query).upsert().updateOne(record.data);
					});
					bulk.execute(function(err, bulkres){
						if (err) return reject(err);
						console.log(bulkres);
						resolve(bulkres);
					});
				});				
			});		
	});
	return promise;
})
.then(function(bulkresult){
	console.log(bulkresult);
	//#console.log('In the final callback::then %d',bulkres.length);
	//process.exit(0);
})
.catch(function(err){
	console.log('in promise err');
	if(err)console.log(err);
});


function performRequest(host, endpoint, method, data, success) {
  var dataString = JSON.stringify(data);
  var headers = {};
  
  if (method == 'GET') {
    endpoint += '?' + querystring.stringify(data);
  }
  else {
    headers = {
      'Content-Type': 'application/json',
      'Content-Length': dataString.length
    };
  }
  var options = {
    host: host,
    path: endpoint,
    method: method,
    headers: headers
  };

  var req = https.request(options, function(res) {
    res.setEncoding('utf-8');

    var responseString = '';

    res.on('data', function(data) {
      responseString += data;
    });

    res.on('end', function() {
      //console.log(responseString);
      var responseObject = JSON.parse(responseString);
      success(responseObject);
    });
  });

  req.write(dataString);
  req.end();
}
